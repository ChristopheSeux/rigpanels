import bpy
from .utils import sort_layer,bone_list,find_mirror,title,get_armature

class RigLayerPanel(bpy.types.Panel) :
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_label = "Rig Layers"

    @classmethod
    def poll(self,context) :
        armatures = [o for o in context.selected_objects if o.type == 'ARMATURE']
        return len(armatures) and len(armatures[0].data.BLayers.layers)
        #return ob and ob.type == 'ARMATURE' and len(ob.data.BLayers.layers)

    @staticmethod
    def draw_header(self, context):
        scene = context.scene
        ob = context.object
        if not context.object.data.library :
            self.layout.prop(scene.BLayers, "layers_settings", text="",icon ='SCRIPTWIN' )

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        ob = get_armature()
        settings = scene.BLayers

        BLayers = ob.data.BLayers


        if settings.layers_settings :
            layout.operator('blayers.add_layer_to_panel',text = 'Add Layers',icon='ZOOMIN')
            #layout.operator('blayers.layer_preset',text = 'Basic').preset = 'BASIC'
            #layout.operator('blayers.layer_preset',text = 'Complet').preset = 'COMPLET'
            row = layout.row(align = True)
            row.operator('blayers.move_rig_layers',icon = 'TRIA_DOWN',text='').operation ='DOWN'
            row.operator('blayers.move_rig_layers',icon = 'TRIA_UP',text='').operation ='UP'
            row.label('')
            row.operator('blayers.move_rig_layers',icon = 'TRIA_DOWN_BAR',text='').operation ='MERGE'
            row.operator('blayers.move_rig_layers',icon = 'TRIA_UP_BAR',text='').operation ='EXTRACT'

            row.separator()
            row.operator('blayers.layer_separator',icon = 'GRIP',text='').operation ='ADD'
            row.operator('blayers.layer_separator',icon = 'PANEL_CLOSE',text='').operation ='REMOVE'

            row.label('')
            row.operator('blayers.move_rig_layers',icon = 'TRIA_LEFT',text='').operation ='LEFT'
            row.operator('blayers.move_rig_layers',icon = 'TRIA_RIGHT',text='').operation ='RIGHT'


        visible_layer = [l for l in BLayers.layers if l.UI_visible]

        col = layout.column(align = True)
        for col_index in sort_layer(visible_layer) :
            col_separator = False
            row = col.row(align = True)
            for l in col_index :
                if l.v_separator :
                    col_separator = True

                if settings.layers_settings and not ob.data.library :
                    row.prop(l,"move",toggle = True,text = l.name)

                else :
                    row.prop(ob.data,"layers",index = l.index,toggle = True,text = l.name)

            if col_separator :
                col.separator()

        row = layout.row(align=True)
        if BLayers.get("presets") :
            for preset in sorted(BLayers["presets"]):
                apply_preset = row.operator('blayers.layer_preset_management',text=preset)
                apply_preset.operation = 'APPLY'
                apply_preset.preset = preset

                if settings.layers_settings :
                    remove = row.operator('blayers.layer_preset_management',icon='ZOOMOUT',text='')
                    remove.operation = 'REMOVE'
                    remove.preset = preset

        if settings.layers_settings :
            row.operator('blayers.layer_preset_management',icon='ZOOMIN',text='').operation = 'ADD'


# Properties
class PropertiesPanel(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_label = "Properties"

    @classmethod
    def poll(self, context):
        return (context.object and context.object.type =='ARMATURE'
                    and context.object.mode == 'POSE' and context.object.data.SnappingChain)

    def is_selected(self,names):
        # Returns whether any of the named bones are selected.
        if type(names) == list:
            for name in names:
                if name in [b.name for b in bpy.context.selected_pose_bones]:
                    return True
        elif names in [b.name for b in bpy.context.selected_pose_bones]:
            return True
        return False

    def is_active(self,names):
        # Returns whether any of the named bones are selected.
        if bpy.context.active_pose_bone :
            if type(names) == list:
                active_bone_name = bpy.context.active_pose_bone
                for name in names:
                    if name == bpy.context.active_pose_bone.name:
                        return True
            elif names == bpy.context.active_pose_bone.name:
                return True

        return False

    def draw(self,context) :
        layout = self.layout

        ob = context.object
        armature = ob.data
        SnappingChain = armature.SnappingChain

        self.bl_label = ob.name.split('_')[0].title()+' Properties'

        selected_bones = []

        if context.selected_pose_bones :
            selected_bones = sorted([bone.name for bone in context.selected_pose_bones])


        for i,chain in enumerate(SnappingChain.IKFK_bones.values()) :
            if chain.IK_last and self.is_active(bone_list(chain)):
                invert = int(not chain.invert_switch)
                ik_last = ob.pose.bones.get(chain.IK_last)

                col = layout.column(align=True)

                #print(chain.switch_prop)

                if chain.switch_prop and ob.path_resolve(chain.switch_prop) == invert :
                    row = col.row(align=True)
                    snapFK = row.operator("snappingchain.snapping",text = 'To FK',icon='COLOR_BLUE')
                    snapFK.chain = repr(chain)
                    snapFK.way = 'to_FK'
                    snapFK.auto_switch = True

                else :
                    row = col.row(align=True)
                    snapIK = row.operator("snappingchain.snapping",text = 'To IK',icon='COLOR_RED')
                    snapIK.chain = repr(chain)
                    snapIK.way = 'to_IK'
                    snapIK.auto_switch = True

                keyframing_chain= row.operator('snappingchain.keyframing_chain',text='',icon ='KEY_HLT' )
                keyframing_chain.chain = repr(chain)
                row.prop(SnappingChain,'IK_option',icon='SCRIPTWIN',text='')

                if SnappingChain.IK_option :
                    box = col.box()
                    box_col = box.column()

                    snapIK = box_col.operator("snappingchain.snapping",text = 'Snap IK --> FK')
                    snapIK.chain = repr(chain)
                    snapIK.way = 'to_IK'
                    snapIK.auto_switch = False

                    snapFK = box_col.operator("snappingchain.snapping",text = 'Snap FK --> IK')
                    snapFK.chain = repr(chain)
                    snapFK.way = 'to_FK'
                    snapFK.auto_switch = False

                    resetIK = box_col.operator("snappingchain.reset_ik",text = 'Reset IK')
                    resetIK.chain = repr(chain)

                    lock_row = box_col.row()
                    lock_row.prop(ik_last,'lock_ik_y',text = 'Y')
                    lock_row.prop(ik_last,'lock_ik_z',text = 'Z')
                    #.box_col.prop(chain,'layer_switch',text = 'FK_IK_layer')


            if chain.get('pin_elbow') and chain.get('target_elbow') :
                if self.is_selected([chain.pin_elbow,chain.IK_tip]):
                    snap_elbow = layout.operator("snappingchain.elbow_snapping",text = 'Snap Elbow',icon='COLOR_RED')
                    snap_elbow.chain = repr(chain)



        for i,bone_prop in enumerate(SnappingChain.space_switch_Bones.values()) :
            bone = ob.pose.bones.get(bone_prop.name)
            if bone and bone.children and self.is_active([b.name for b in bone.children]):
                #col = layout.column(align = True)
                #row = col.row()
                #row.label('Current Space :%s'%'test')
                space_switch_bone = bone.parent
                space = [c.name for c in bone.constraints][bone.children[0]["space"]]
                layout.operator("snappingchain.switch_space",text='Switch Space : (%s)'%space)
                #{layout.prop(bone_prop,'space')

        #sorted_bones = []
        #for bone in selected_bones :
        #    pass

        #display custom prop :
        for bone in selected_bones :
            bone = ob.pose.bones.get(bone)
            if bone.keys() :
                col = layout.column(align =True)
                for key in bone.keys() :
                    if not key.startswith('_') :
                        if bone.name.endswith(('.R','.L')) :
                            side = bone.name.split('.')[-1]
                            text = title(key)+' (%s) '%side
                            col.prop(bone,'["%s"]'%key,text = text)

                        else :
                            text = title(key)
                            col.prop(bone,'["%s"]'%key,text = text)
