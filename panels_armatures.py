import bpy

class SnappingChainPanel(bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"
    bl_label = "Snapping Chain"

    @classmethod
    def poll(self, context):
        return (context.object and context.object.type =='ARMATURE')

    def bone_field(self,layout,ob,chain,prop,text=None)  :
        subrow = layout.row(align=True)
        if text :
            subrow.prop_search(chain,prop,ob.pose,'bones',text=text)
        else :
            subrow.prop_search(chain,prop,ob.pose,'bones')
        eyedrop = subrow.operator("snappingchain.bone_eyedropper",text='',icon = 'EYEDROPPER')
        eyedrop.field = repr(chain)
        eyedrop.prop = prop

    def draw(self,context) :
        layout = self.layout

        ob = context.object
        armature = ob.data
        SnappingChain = armature.SnappingChain

        main_column = layout.column(align=True)

        tab = main_column.row(align=True)
        tab.prop(SnappingChain,'snap_type',expand=True)

        if SnappingChain.snap_type == 'IK_FK' :
            tools_row = main_column.row(align=True)
            addChain = tools_row.operator("snappingchain.add_remove_field",text='Add FK IK Chain',icon = 'GROUP_BONE')
            addChain.values=str({'set':{'expand':True},'add':True,'prop':repr(SnappingChain.IKFK_bones)})
            tools_row.operator("snappingchain.copy_layers",text='',icon = 'COPYDOWN')
            tools_row.operator("snappingchain.paste_layers",text='',icon = 'PASTEDOWN')

            chains_column = main_column.column(align = True)

            for i,chain in enumerate(SnappingChain.IKFK_bones.values()) :
                box = chains_column.box()
                row = box.row(align=False)

                row.prop(chain,'expand',text='',icon = 'TRIA_DOWN' if chain.expand else 'TRIA_RIGHT',emboss=False)
                row.prop(chain,'name',text='')

                mirror_chain = row.operator("snappingchain.mirror_chain",icon ='MOD_MIRROR',text='')
                mirror_chain.index =i

                subrow = row.row(align=True)
                remove_bone = subrow.operator("snappingchain.add_remove_field",icon='ZOOMOUT',text='')
                remove_bone.values = str({'add':False,'prop':repr(chain.FK_mid),'index':len(chain.FK_mid)-1})

                add_bone = subrow.operator("snappingchain.add_remove_field",icon='ZOOMIN',text='')
                add_bone.values = str({'add':True,'prop':repr(chain.FK_mid)})

                removeChain = row.operator("snappingchain.add_remove_field",text='',icon = 'X',emboss = False)
                removeChain.values = str({'add':False,'prop':repr(SnappingChain.IKFK_bones),'index':i})


                if chain.expand :

                    #row = box.row(align=True)
                    col = box.column(align = True)
                    self.bone_field(col,ob,chain,'FK_root')

                    for j,bone in enumerate(chain.FK_mid) :
                        self.bone_field(col,ob,bone,'name',text='FK_mid_%02d'%(j+1))

                    self.bone_field(col,ob,chain,'FK_tip')

                    col.separator()
                    self.bone_field(col,ob,chain,'IK_last')

                    '''
                    for bone in chain.IK_mid :
                        self.bone_field(col,ob,bone,'name')
                        '''

                    for prop in ['IK_tip','IK_pole'] :
                        self.bone_field(col,ob,chain,prop)

                    col.separator()

                    subrow = col.row(align= True)
                    subrow.operator("snappingchain.auto_ikfklayers",text='',icon = 'RENDERLAYERS').index = i
                    subrow.prop(chain,'FK_layer', text = 'FK_layer')
                    subrow.prop(chain,'IK_layer',text = 'IK_layer')

                    col.separator()
                    subrow = col.row()

                    subrow.prop(chain,'switch_prop')
                    subrow.prop(chain,'invert_switch',text='')



                    col.separator()

                    box = col.box()
                    box_col = box.column()
                    subrow = box_col.row(align=True)
                    subrow.prop(chain,"extra_settings",text='',icon = 'TRIA_DOWN' if chain.extra_settings else 'TRIA_RIGHT',emboss = False)
                    subrow.label('Extra settings')


                    if chain.extra_settings :

                        subrow = box_col.row(align=True)
                        subrow.prop_search(chain,'IK_stretch_last',ob.pose,'bones')
                        eyedrop=subrow.operator("snappingchain.bone_eyedropper",text='',icon = 'EYEDROPPER')
                        eyedrop.field = repr(chain)
                        eyedrop.prop = 'IK_stretch_last'

                        subrow = box_col.row(align=True)
                        subrow.prop_search(chain,'pin_elbow',ob.pose,'bones')
                        eyedrop=subrow.operator("snappingchain.bone_eyedropper",text='',icon = 'EYEDROPPER')
                        eyedrop.field = repr(chain)
                        eyedrop.prop = 'pin_elbow'

                        subrow = box_col.row(align=True)
                        subrow.prop_search(chain,'target_elbow',ob.pose,'bones')
                        eyedrop=subrow.operator("snappingchain.bone_eyedropper",text='',icon = 'EYEDROPPER')
                        eyedrop.field = repr(chain)
                        eyedrop.prop = 'target_elbow'

                        #subrow = box_col.row(align=True)
                        box_col.prop(chain,'full_snapping')



        if SnappingChain.snap_type == 'SPACE_SWITCH' :
            add_bone = main_column.operator("snappingchain.add_remove_field",text='Add space switch bones')
            add_bone.values = str({'add':True,'prop':repr(SnappingChain.space_switch_Bones)})
            #add_bones.prop = repr(SnappingChain.space_switch_Bones)
            #add_bones.add = True
            box = main_column.box()
            col = box.column()
            for j,bone in enumerate (SnappingChain.space_switch_Bones) :
                row = col.row(align=True)
                row.prop_search(bone,'name',ob.pose,'bones',text= '')
                eyedrop=row.operator("snappingchain.bone_eyedropper",text='',icon = 'EYEDROPPER')
                eyedrop.field = repr(bone)
                eyedrop.prop = 'name'
                #eyedrop.bone = j

                remove_bone = row.operator("snappingchain.add_remove_field",text='',icon ='X')
                remove_bone.values = str({'index':j,'add':False,'prop':repr(SnappingChain.space_switch_Bones)})
                #remove_bone.prop = repr(SnappingChain.space_switch_Bones)
                #remove_bone.index = j
                #remove_bone.add = False
