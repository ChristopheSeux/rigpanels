import bpy
import copy
from .utils import *
from .functions import flatten_layer_tree


def lock_layers(self,context):
    ob = get_armature()
    ob_mode = ob.mode
    active_ob = context.scene.objects.active
    active_ob_mode = active_ob.mode

    BLayers = self.id_data.BLayers.layers

    bpy.ops.object.mode_set(mode ='OBJECT')

    for b in [b for b in ob.data.bones if b.layers[self.index]] :
        b.hide_select = self.lock
        if self.lock:
            b.select = False

    context.scene.objects.active = ob
    bpy.ops.object.mode_set(mode ='EDIT')
    for b in [b for b in ob.data.edit_bones if b.layers[self.index]] :
        b.hide_select = self.lock
        if self.lock:
            b.select = False

    bpy.ops.object.mode_set(mode =ob_mode)

    context.scene.objects.active = active_ob
    bpy.ops.object.mode_set(mode =active_ob_mode)


def lock_group_layers(self,context):
    BLayers = self.id_data.BLayers.layers
    layer_tree = eval(self.id_data.BLayers["layer_tree"])
    flat_layer_tree = flatten_layer_tree(layer_tree,[],{})

    layers_indices = {l['id'] : l for i,l in enumerate(BLayers)}

    for id in flat_layer_tree[self["id"]]['children_direct'] :
        l = layers_indices[id]

        if l["type"] == 'LAYER' :
            l.lock = self.lock_group

        elif l["type"] == 'GROUP' :
            l.lock_group = self.lock_group

def hide_group_layers(self,context) :
    BLayers = self.id_data.BLayers.layers
    layer_tree = eval(self.id_data.BLayers["layer_tree"])
    flat_layer_tree = flatten_layer_tree(layer_tree,[],{})

    layers_indices = {l['id'] : l for i,l in enumerate(BLayers)}

    for id in flat_layer_tree[self["id"]]['children'] :
        l = layers_indices[id]
        #l.visibility = self.visibility

        if l["type"] == 'LAYER' :
            self.id_data.layers[l.index] = self.visibility
        elif l["type"] == 'GROUP' :
            l.visibility = self.visibility

class LayersSettings(bpy.types.PropertyGroup):
    lock = bpy.props.BoolProperty(update = lock_layers,description = 'Lock all objects on this layer')
    lock_group = bpy.props.BoolProperty(update = lock_group_layers,description = 'Lock all layers in this group')

    move = bpy.props.BoolProperty()
    index = bpy.props.IntProperty(default = -1)
    #type = bpy.props.StringProperty(default = 'LAYER')
    visibility = bpy.props.BoolProperty(default = True,update=hide_group_layers,description = 'Hide all layers in this group')

    expand = bpy.props.BoolProperty(default = True,description = 'Expand this group')
    #id = bpy.props.IntProperty(default = -1)
    col_index = bpy.props.IntProperty()

    column = bpy.props.IntProperty(default = 0)
    row = bpy.props.IntProperty(default = 0)

    UI_visible = bpy.props.BoolProperty(default = False)
    v_separator = bpy.props.BoolProperty(default = False)

'''
def update_layers_preset(self,context):
    BLayers = context.object.data.BLayers
    layers_to_show =  [l for l in BLayers["presets"][BLayers.layers_preset_enum]]
    for i,l in enumerate(context.object.data.layers):
        if i in layers_to_show :
            context.object.data.layers[i] = True
        else :
            context.object.data.layers[i] = False

def layers_preset_items(self,context):
    presets = context.object.data.BLayers["presets"]
    return [(p.upper(),p.title(),"",i) for i,p in enumerate(sorted(presets))]
    '''


class BLayersArmature(bpy.types.PropertyGroup) :
    filter = bpy.props.StringProperty(options ={"TEXTEDIT_UPDATE"})
    layers = bpy.props.CollectionProperty(type = LayersSettings)
    active_index = bpy.props.IntProperty()
    """
    def get_layer_tree(self) :
        ob = get_armature()
        layer_tree = eval(ob.data.BLayers["layer_tree"])

        return flatten_layer_tree(layer_tree,parent_info={'parents':[],'level':-1,"children":[]},flat_list={})
        """


class BLayersScene(bpy.types.PropertyGroup) :
    show_index = bpy.props.BoolProperty(default = False)
    layers_settings = bpy.props.BoolProperty(default = False)

    icons = bpy.utils.previews.new()




icons_dir = join(dirname(__file__),'icons')
for icon in listdir(icons_dir) :
    BLayersScene.icons.load(splitext(icon)[0].upper(), join(icons_dir, icon), 'IMAGE',force_reload=True)


## Snapping Properties
class SpaceSwitchSettings(bpy.types.PropertyGroup) :
    '''
    def update_space(self,context) :
        rig = context.object
        bone = context.active_pose_bone
        space_switch_bone = bone.parent

        world_mat = bone.matrix.copy()

        bone["space"] = [c.name for c in space_switch_bone.constraints].index(self.space)
        rig.update_tag({'OBJECT'})
        bpy.context.scene.update()

        bone.matrix = world_mat

        if context.scene.tool_settings.use_keyframe_insert_auto :
            context.object.keyframe_insert('pose.bones["%s"]["space"]'%bone.name)
            if bone.rotation_mode == 'QUATERNION' :
                mode = 'rotation_quaternion'
            elif bone.rotation_mode == 'AXIS_ANGLE' :
                mode = 'rotation_axis_angle'
            else :
                mode = 'rotation_euler'
            context.object.keyframe_insert('pose.bones["%s"].location'%(bone.name))
            context.object.keyframe_insert('pose.bones["%s"].%s'%(bone.name,mode))
            context.object.keyframe_insert('pose.bones["%s"].scale'%(bone.name))

    def items_space(self,context):
        items = []
        bone = context.active_pose_bone

        space_switch_bone = bone.parent

        for constraint in space_switch_bone.constraints :
            items.append((constraint.name,constraint.name,''))

        return items
        '''

    name = bpy.props.StringProperty()
    #space = bpy.props.EnumProperty(items = items_space,update= update_space)



class IKFKSettings(bpy.types.PropertyGroup) :
    bone = bpy.props.CollectionProperty(type = bpy.types.PropertyGroup)

    expand = bpy.props.BoolProperty()

    FK_root = bpy.props.StringProperty()
    FK_mid = bpy.props.CollectionProperty(type = bpy.types.PropertyGroup)
    FK_tip = bpy.props.StringProperty()

    IK_last = bpy.props.StringProperty()
    IK_tip = bpy.props.StringProperty()
    IK_pole = bpy.props.StringProperty()

    FK_layer = bpy.props.IntProperty()
    IK_layer = bpy.props.IntProperty()

    switch_prop = bpy.props.StringProperty()
    invert_switch = bpy.props.BoolProperty(default = False,description='Invert the switch property')
    extra_settings = bpy.props.BoolProperty()

    pin_elbow = bpy.props.StringProperty()
    target_elbow = bpy.props.StringProperty()

    IK_stretch_last = bpy.props.StringProperty()

    full_snapping = bpy.props.BoolProperty()


class SnappingChainSettings(bpy.types.PropertyGroup) :
    #constraints = bpy.props.StringProperty()
    IK_option = bpy.props.BoolProperty()
    IKFK_bones = bpy.props.CollectionProperty(type = IKFKSettings)
    space_switch_Bones = bpy.props.CollectionProperty(type = SpaceSwitchSettings)
    snap_type = bpy.props.EnumProperty(items = [(i,title(i),"") for i in ("IK_FK","SPACE_SWITCH")])
