import bpy
from .utils import split_path


def get_group(ob,group_name) :
    if isinstance(ob,bpy.types.PoseBone) :
        ob = ob.id_data

    group = ob.animation_data.action.groups.get(group_name)

    if not group :
        group = ob.animation_data.action.groups.new(group_name)

    print(group)
    return group

def get_fcurve(ob,data_path,index=-1) :
    import re

    if not ob.animation_data :
        ob.animation_data_create()

    if not ob.animation_data.action :
        name = re.sub(r'[\._-]+rig$', '_proxy_action', ob.name, flags=re.I)
        action = bpy.data.actions.new(name)
        ob.animation_data.action = action

    fc = ob.animation_data.action.fcurves.find(data_path,index=index)
    found = True
    if not fc :
        fc = ob.animation_data.action.fcurves.new(data_path,index)
        found = False

    return fc,found



def get_transform_channel(ob,exclude_locked=True) :
    channels = {"location":[],"scale":[]}

    #location
    for i,channel in enumerate(ob.location) :
        if not exclude_locked or not ob.lock_location[i] :
            channels["location"].append(i)

    # Rotation
    if ob.rotation_mode in ["QUATERNION","AXIS_ANGLE"] :
        rotation_channel_name = 'rotation_'+ob.rotation_mode.lower()
        channels[rotation_channel_name]=[]

        if not exclude_locked or not ob.lock_rotation_w :
            channels[rotation_channel_name].append(0)
        for i in range (0,3) :
            if not exclude_locked or not ob.lock_rotation[i] :
                channels[rotation_channel_name].append(i+1)

    else :
        rotation_channel_name = "rotation_euler"
        rotation_channel = getattr(ob,rotation_channel_name)

        channels[rotation_channel_name]=[]

        for i,channel in enumerate(rotation_channel) :
            if not exclude_locked or not ob.lock_rotation[i] :
                channels[rotation_channel_name].append(i)

    #Scale
    for i,channel in enumerate(ob.scale) :
        if not exclude_locked or not ob.lock_scale[i] :
            channels["scale"].append(i)

    #print(channels)
    return channels


def insert_keyframe(ob,custom_prop=True,transform = True,data_paths=[],frame = None,only_available = False,interpolation = None,exclude_locked = True):
    """ Insert Keyframe on selected_bones channel except for the locked one,
        insert key in custom prop too.
    """
    if not interpolation :
        interpolation = bpy.context.user_preferences.edit.keyframe_new_interpolation_type

    color_mode = 'AUTO_RGB'

    base_ob = ob.id_data
    bone = ob if isinstance(ob,bpy.types.PoseBone) else None

    if not frame :
        frame = bpy.context.scene.frame_current

    transform_channels = get_transform_channel(ob,exclude_locked)

    ## Transform Keyframes
    for channel, indexes in transform_channels.items() :
        for index in indexes :
            data_path ='pose.bones["%s"].%s'%(bone.name,channel) if bone else channel
            group_name =  bone.name if bone else channel.replace('_',' ').title()

            fc,found = get_fcurve(base_ob,data_path,index)
            if found and frame !=  bpy.context.scene.frame_current :
                value = fc.evaluate(frame)
            else :
                value = base_ob.path_resolve(data_path)[index]

            fc.group = get_group(ob,group_name)
            fc.color_mode = color_mode

            point = fc.keyframe_points.insert(frame,value)
            point.interpolation = interpolation

    ## Custom Prop
    if custom_prop == True :
        for key,value in ob.items() :
            if key != '_RNA_UI' and key and type(value) in (int,float,bool):

                data_path ='pose.bones["%s"]["%s"]'%(bone.name,key) if bone else '["%s"]'%key

                fc,found = get_fcurve(base_ob,data_path)
                if found and frame !=  bpy.context.scene.frame_current :
                    value = fc.evaluate(frame)
                else :
                    value = base_ob.path_resolve(data_path)

                if bone :
                    fc.group = get_group(ob,bone.name)
                fc.color_mode = color_mode

                point = fc.keyframe_points.insert(frame,value)
                point.interpolation = interpolation

    # Data path
    for data_path in data_paths :
        bone_name,prop = split_path(data_path)

        fc,found = get_fcurve(base_ob,data_path)
        if found and frame !=  bpy.context.scene.frame_current :
            value = fc.evaluate(frame)
        else :
            value = base_ob.path_resolve(data_path)

        fc.group = get_group(ob,bone_name)
        fc.color_mode = color_mode

        point = fc.keyframe_points.insert(frame,value)
        point.interpolation = interpolation


    try :
        for area in bpy.context.screen.areas :
            area.tag_redraw()
    except :
        pass
