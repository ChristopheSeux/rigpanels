import bpy

from .functions import *
from .utils import *

class LayerPresetManagment(bpy.types.Operator) :
    """Layer Preset Management"""
    bl_idname = "blayers.layer_preset_management"
    bl_label = "Add or Remove Layers Preset"

    operation = bpy.props.StringProperty()
    preset = bpy.props.StringProperty()
    preset_name = bpy.props.StringProperty()

    def draw(self,context) :
        layout = self.layout

        layout.prop(self,"preset_name")

    def execute(self, context):
        operation = self.operation
        ob = context.object

        BLayers = ob.data.BLayers

        sorted_layers = sort_layer([l for l in BLayers.layers if l.UI_visible])
        selected_layers =  [l for l in BLayers.layers if l['type'] == 'LAYER' and l.move]

        if not BLayers.get("presets"):
            BLayers["presets"] = {}

        if operation == 'ADD' :
            BLayers["presets"][self.preset_name] = [l.index for l in BLayers.layers if l.UI_visible and l.move]

        elif operation == 'REMOVE' :
            BLayers["presets"].pop(self.preset)

        elif operation == 'APPLY' :
            layers_to_show =  list(BLayers["presets"][self.preset])

            ob.data.layers = [i in layers_to_show for i in range(0,32)]


        redraw_areas()
        return {'FINISHED'}

    def invoke(self,context,event) :
        wm = context.window_manager
        if self.operation == 'ADD' :
            return wm.invoke_props_dialog(self)
        else :
            return self.execute(context)

class LayerSeparator(bpy.types.Operator) :
    """Layer Separator"""
    bl_idname = "blayers.layer_separator"
    bl_label = "Add Layer separator"

    operation = bpy.props.StringProperty()

    def execute(self, context):
        operation = self.operation
        ob = context.object

        BLayers = ob.data.BLayers.layers

        sorted_layers = sort_layer([l for l in BLayers if l.UI_visible])
        selected_layers =  [l for l in BLayers if l['type'] == 'LAYER' and l.move]

        for l in selected_layers :
            l.v_separator = operation == 'ADD'
            print(operation,operation == 'ADD')

        return {'FINISHED'}


class MoveRigLayers(bpy.types.Operator) :
    """Move rig layers"""
    bl_idname = "blayers.move_rig_layers"
    bl_label = "Move Rig Layers"

    operation = bpy.props.StringProperty()


    def execute(self, context):
        operation = self.operation
        ob = context.object

        BLayers = ob.data.BLayers.layers

        sorted_layers = sort_layer([l for l in BLayers if l.UI_visible])
        selected_layers =  [l for l in BLayers if l['type'] == 'LAYER' and l.move]

        if operation == 'UP' :
            for i,row in enumerate(sorted_layers) :
                index = row[0].column
                above_layer = sorted_layers[i-1]
                above_index = above_layer[0].column

                if len([l for l in BLayers if l.column == index and l.move]): # if at least one layer in the row is selected
                    for layer in row :
                        layer.column = above_index

                    for layer in above_layer :
                        layer.column = index
                    break


        elif operation == 'DOWN' :
            for i,row in enumerate(sorted_layers) :
                index = row[0].column
                above_layer = sorted_layers[i+1]
                above_index = above_layer[0].column

                if len([l for l in BLayers if l.column == index and l.move]): # if at least one layer in the row is selected
                    for layer in row :
                        layer.column = above_index

                    for layer in above_layer :
                        layer.column = index
                    break

        elif operation == 'LEFT' :
            for row in sorted_layers :
                for i,l in enumerate(row) :
                    if l.move :
                        index = l.row
                        left_layer = row[i-1]
                        l.row = left_layer.row
                        left_layer.row = index

        elif operation == 'RIGHT' :
            for row in sorted_layers :
                for i,l in enumerate(row) :
                    if l.move :
                        index = l.row
                        left_layer = row[i+1]
                        l.row = left_layer.row
                        left_layer.row = index

        elif operation == 'MERGE' :
            for i,row in enumerate(sorted_layers) :
                for l in row :
                    if l.move :
                        print('### Merge')
                        print([l.row for l in sorted_layers[i+1]])

                        l.row = max([l.row for l in sorted_layers[i+1]])+1
                        l.column = sorted_layers[i+1][0].column


        elif operation == 'EXTRACT' :
            for i,row in enumerate(sorted_layers) :
                if len([l for l in row if l.move]) : # if a layer is selected
                    for below_row in [r for j,r in enumerate(sorted_layers) if j>i] :
                        for l in below_row :
                            l.column+=1
                    for l in [l for l in row if not l.move] :
                        l.column+=1
                        l.row = 0


        return {'FINISHED'}

        '''
    def invoke(self, context, event):
        scene = context.scene
        ob = context.object
        layout = self.layout

        BLayers = ob.data.BLayers.layers
        for l in [l for l in BLayers if l.type == 'LAYER'] :
            l.move = False

        return self.execute(context)
        '''


class AddLayerToPanel(bpy.types.Operator) :
    """Add layer to the right panel"""
    bl_idname = "blayers.add_layer_to_panel"
    bl_label = "Add Layer to the right panel"

    @classmethod
    def poll(self,context) :
        return context.object.type == 'ARMATURE'


    def draw(self,context) :
        scene = context.scene
        ob = context.object
        layout = self.layout
        BLayers = ob.data.BLayers.layers

        col = layout.column(align=True)
        for l in [l for l in BLayers if l['type'] == 'LAYER'] :
            col.prop(l,'move',toggle = True,text = l.name)

    def execute(self, context):
        scene = context.scene
        ob = context.object
        BLayers = ob.data.BLayers.layers

        for l in BLayers :
            if l.move :
                if not l.UI_visible :
                    l.UI_visible= True
                    l.column = max([l.column for l in BLayers if l['type'] == 'LAYER'])+1
            else :
                l.UI_visible= False
                l.column = 0


        redraw_areas()
        return {'FINISHED'}

    def invoke(self, context, event):
        scene = context.scene
        ob = context.object
        layout = self.layout

        BLayers = ob.data.BLayers.layers

        for l in BLayers :
            l.move = l.UI_visible

        wm = context.window_manager
        return wm.invoke_props_dialog(self,width=150)



class CopyLayers(bpy.types.Operator):
    """Change BLayers to clipboard"""
    bl_idname = "blayers.copy_layers"
    bl_label = "Copy BLayer"

    def execute(self, context):
        ob = get_armature()

        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers

        Layers = {}
        if BLayersSettings.get("presets") :
            Layers['presets'] = BLayersSettings['presets'].to_dict()
        Layers['objects'] = {}
        for ob in ob.data.bones :
            Layers['objects'][ob.name] = list(ob.layers)

        Layers['layers'] = []
        for l in BLayers :
            Layers['layers'].append({   'name' :l.name,
                                        'type':l['type'],
                                        'index':l.index,
                                        'id' : l['id'],
                                        'column':l.column,
                                        'row' : l.row,
                                        'UI_visible':l.UI_visible,
                                        'v_separator':l.v_separator})

        Layers['layer_tree'] = BLayersSettings["layer_tree"]


        context.window_manager.clipboard = str(Layers)

        return {'FINISHED'}

class PasteLayers(bpy.types.Operator):
    """Past BLayers from clipboard"""

    bl_idname = "blayers.paste_layers"
    bl_label = "Copy BLayer"

    def execute(self, context):
        ob = get_armature()
        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers
        #layers_from,BLayers,BLayersSettings,layers,objects,selected = source_layers()

        #delete_layer
        for i in range(len(BLayers)) :
            #print(i)
            BLayers.remove(0)

        try :
            Layers = eval(context.window_manager.clipboard)
        except :
            self.report({'ERROR'},"Wrong ClipBoard")
            return {'FINISHED'}

        if Layers.get('presets') :
            BLayersSettings['presets'] = Layers['presets']

        for o,layers in Layers['objects'].items() :
            bone = ob.data.bones.get(o)
            if bone:
                bone.layers = layers

        for layer_info in Layers['layers'] :
            layer = BLayers.add()
            for attr,value in layer_info.items() :
                if hasattr(layer, attr) :
                    setattr(layer,attr,value)
                else :
                    layer[attr] = value

        BLayersSettings['layer_tree'] = Layers["layer_tree"]


        redraw_areas()



        return {'FINISHED'}


class SynchroniseLayers(bpy.types.Operator):
    """Create missings layers"""
    bl_idname = "blayers.synchronise_layers"
    bl_label = "Synchronise Layers"

    def execute(self, context):
        scene = context.scene
        ob = get_armature()

        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers
        layers = ob.data.layers

        full_layers = []
        existing_indexes = [l.index for l in BLayers if l['type'] =='LAYER']

        for b in ob.data.bones :
            for i,l in enumerate(b.layers) :
                if l and i not in full_layers:
                    full_layers.append(i)


        for free_index in full_layers :
            if not free_index in existing_indexes :
                new_layer = layer_add(BLayers,'LAYER',free_index)


        redraw_areas()

        return {'FINISHED'}


class ObjectsToLayer(bpy.types.Operator):
    """Move objects to layers"""
    bl_idname = "blayers.objects_to_layer"
    bl_label = "Objects To Layer"

    layer_index = bpy.props.IntProperty()
    new_layer = bpy.props.BoolProperty(default = False)

    @classmethod
    def poll(self,context) :
        return True if get_selected_bones() and len(context.object.data.BLayers.layers) else False

    def draw(self,context) :
        scene = context.scene
        layout = self.layout

        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers

        col = layout.column(align=True)
        for l in [l for l in BLayers if l.type == 'LAYER'] :
            col.prop(l,'move',toggle = True,text = l.name)

        col.separator()
        col.prop(self,"new_layer",text = "New Layer",toggle=True)
        #icon = utils.custom_icons["NEW_LAYER_OBJECT"].icon_id
        #layout.operator("blayers.add_layer", icon_value=icon,text = 'Add layer from object').type = 'LAYER_FROM_SELECTED'


    def execute(self, context):
        scene = context.scene
        ob = context.object
        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers

        index = BLayersSettings.layers[BLayersSettings.active_index].index

        for b in get_selected_bones() :
            b.layers[index] = True

            print(self.shift)
            if not self.shift :
                for i in self.src_layers :
                    b.layers[i]=False

        return {'FINISHED'}

    def invoke(self, context, event):
        scene = context.scene
        ob = context.object
        BLayersSettings = ob.data.BLayers
        index = BLayersSettings.layers[BLayersSettings.active_index].index

        self.shift = event.shift

        self.src_layers = []
        for b in get_selected_bones() :
            for i,l in enumerate(b.layers) :
                if l and i != index :
                    self.src_layers.append(i)


        return self.execute(context)



class SelectObjects(bpy.types.Operator):
    """Select objects on layer"""
    bl_idname = "blayers.select_objects"
    bl_label = "Select all objects on active layer"

    def execute(self, context):
        scene = context.scene
        ob = context.object

        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers

        active_layer = BLayers[BLayersSettings.active_index]
        active_index = active_layer.index

        if active_layer['type'] == 'LAYER' :
            layers = [active_index]
        else :
            layers = [l.index for l in BLayers if l.id == active_layer.id]

        for b in ob.data.bones :
            for index in layers :
                if b.layers[index] :
                    b.select = True
                    ob.data.bones.active = b


        #update_col_index(BLayers)
        return {'FINISHED'}

class ToggleLayerLock(bpy.types.Operator):
    """Isolate property"""
    bl_idname = "blayers.toogle_layer_lock"
    bl_label = "Toggle Layer Render"

    def execute(self, context):
        scene = context.scene
        ob = get_armature()

        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers
        active_layer = BLayers[BLayersSettings.active_index]

        other_layers = [l for l in BLayers if l!=active_layer]

        toogle = [not l.lock for l in other_layers]

        active_layer_toogle = False
        layers_toogle = False

        if any(toogle) :
            layers_toogle = True

        for l in other_layers:
            if l['type'] == 'LAYER' :
                setattr(l, "lock",layers_toogle)
            elif l['type'] == 'GROUP' :
                setattr(l, "lock_group",layers_toogle)

        if l['type'] == 'LAYER' :
            setattr(active_layer, "lock", active_layer_toogle)

        elif l['type'] == 'GROUP' :
            setattr(active_layer, "lock_group", active_layer_toogle)

        return {'FINISHED'}

class ToggleLayerLock(bpy.types.Operator):
    """Isolate property"""
    bl_idname = "blayers.toogle_layer_lock"
    bl_label = "Toggle Layer Render"

    def get_lock(self,l):
        ob = get_armature()

        if l["type"] == 'GROUP' :
            return l.lock_group
        elif l["type"] == "LAYER":
            return l.lock

    def set_lock(self,l,toogle):
        ob = get_armature()

        if l["type"] == 'GROUP' :
            l.lock_group = toogle
        elif l["type"] == "LAYER":
            l.lock = toogle

    def execute(self, context):
        scene = context.scene
        ob = get_armature()

        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers
        active_layer = BLayers[BLayersSettings.active_index]

        layer_tree = eval(BLayersSettings["layer_tree"])
        flat_layer_tree = flatten_layer_tree(layer_tree,[],{})

        children = flat_layer_tree[active_layer["id"]]["children"]

        other_layers = [l for l in BLayers if l!=active_layer and l['id'] not in children]

        toogle = [not self.get_lock(l) for l in other_layers]

        active_layer_toogle = False
        layers_toogle = False


        if any(toogle) :
            layers_toogle = True

        for l in other_layers:
            self.set_lock(l,layers_toogle)

        self.set_lock(active_layer,active_layer_toogle)

        return {'FINISHED'}

class ToggleLayerHide(bpy.types.Operator):
    """Isolate property"""
    bl_idname = "blayers.toogle_layer_hide"
    bl_label = "Toggle Layer Render"

    def get_visibility(self,l):
        ob = get_armature()

        if l["type"] == 'GROUP' :
            return l.visibility
        elif l["type"] == "LAYER":
            return ob.data.layers[l.index]

    def set_visibility(self,l,toogle):
        ob = get_armature()

        if l["type"] == 'GROUP' :
            l.visibility = toogle
        elif l["type"] == "LAYER":
            ob.data.layers[l.index] = toogle

    def execute(self, context):
        scene = context.scene
        ob = get_armature()

        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers
        active_layer = BLayers[BLayersSettings.active_index]

        layer_tree = eval(BLayersSettings["layer_tree"])
        flat_layer_tree = flatten_layer_tree(layer_tree,[],{})

        children = flat_layer_tree[active_layer["id"]]["children"]

        other_layers = [l for l in BLayers if l!=active_layer and l['id'] not in children]

        toogle = [self.get_visibility(l) for l in other_layers]

        active_layer_toogle = True
        layers_toogle = True


        if any(toogle) :
            layers_toogle = False

        for l in other_layers:
            self.set_visibility(l,layers_toogle)

        self.set_visibility(active_layer,active_layer_toogle)



        """

        active_layer = BLayers[BLayersSettings.active_index]
        passive_layers = [l for l in BLayers if l !=active_layer]

        prop = self.prop

        if active_layer['type'] == 'GROUP' :
            passive_layers = [l for l in BLayers if l['type'] =='LAYER' and l !=active_layer and l['id'] != active_layer['id']]
        else :
            passive_layers = [l for l in BLayers if l['type'] =='LAYER' and l !=active_layer]

        true_layers = [l for l in BLayers if l !=active_layer and l['type'] =='LAYER' and getattr(l,prop)]
        toogle = False if len(true_layers)==len(passive_layers) else True

        groups = [g for g in BLayers if g['type'] =='GROUP' and g['id'] != active_layer['id']]
        #scene.layers[active_index] = True

        setattr(active_layer,prop,False)

        for g in groups :
            setattr(g,prop,toogle)

        for l in passive_layers:
            if l['type'] == 'LAYER' :
                setattr(l,prop,toogle)

        """
        return {'FINISHED'}


class MoveLayer(bpy.types.Operator):
    """Move Layer up or down"""
    bl_idname = "blayers.layer_move"
    bl_label = "Move Layer"

    step = bpy.props.IntProperty()

    def execute(self, context):
        scene = context.scene
        ob = get_armature()

        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers
        col_index = BLayersSettings.active_index
        layer = BLayers[col_index]

        layer_tree = eval(ob.data.BLayers["layer_tree"])
        flat_tree = flatten_layer_tree(layer_tree, [], {})

        index = flat_tree[layer["id"]]["index"]

        #print(layer.name,layer["id"])
        if 0 <= index+self.step < len(BLayers):

            ob.data.BLayers["layer_tree"] = str(move_item(layer_tree,layer["id"],self.step))


        redraw_areas()
        return {'FINISHED'}

class AddLayer(bpy.types.Operator):
    """Add Layer"""
    bl_idname = "blayers.add_layer"
    bl_label = "Add Gpencil Layer"

    type = bpy.props.StringProperty()

    def execute(self, context):
        scene = context.scene
        ob = get_armature()

        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers
        layers = ob.data.layers

        new_layer = layer_add(BLayers,self.type)

        layers_indices = {l['id'] : i for i,l in enumerate(BLayers)}

        BLayersSettings.active_index = layers_indices[new_layer["id"]]


        if self.type == 'LAYER_FROM_SELECTED' :
            bone_layers = [False]*len(layers)
            bone_layers[new_layer.index] = True
            for bone in get_selected_bones() :
                ob.layers = empty_layers

        redraw_areas()

        return {'FINISHED'}

class RemoveLayer(bpy.types.Operator):
    """Remove Layer"""
    bl_idname = "blayers.remove_layer"
    bl_label = "Remove Gpencil Layer"

    def execute(self, context):
        scene = context.scene
        ob = context.object

        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers

        active_index = BLayersSettings.active_index
        active_layer = BLayers[active_index]

        active_id = active_layer["id"]

        layer_tree = eval(BLayersSettings["layer_tree"])
        flatten_tree = flatten_layer_tree(layer_tree,[],{})

        for c in flatten_tree[active_id]["children"] :
            layer_index = {l["id"]:i for i,l in enumerate(BLayers)}
            BLayers.remove(layer_index[c])


        BLayersSettings["layer_tree"] = str(remove_item(layer_tree,active_id))
        BLayers.remove(active_index)


        layers_indices = {v["index"]:k for k,v in flatten_tree.items()}
        above_id = layers_indices[flatten_tree[active_id]['index']-1]

        layers_indices = {l['id'] : i for i,l in enumerate(BLayers)}
        if layers_indices :
            BLayersSettings.active_index = layers_indices[above_id]
        else :
            BLayersSettings.active_index = 0


        redraw_areas()
        return {'FINISHED'}


class MoveInGroup(bpy.types.Operator):
    """Put layer in the group"""
    bl_idname = "blayers.move_in_group"
    bl_label = "Add Gpencil Layer"

    index = bpy.props.IntProperty()

    def execute(self, context):
        scene = context.scene
        ob = get_armature()

        BLayersSettings = ob.data.BLayers
        BLayers = BLayersSettings.layers
        active_index = BLayersSettings.active_index
        layer = BLayers[active_index]

        if self.index == active_index :
            return {'FINISHED'}

        layer_tree = eval(ob.data.BLayers["layer_tree"])

        layer_tree['children'] = move_item_in_group(layer_tree['children'],layer["id"],BLayers[self.index]["id"])

        BLayersSettings["layer_tree"] = str(layer_tree)


        redraw_areas()

        return {'FINISHED'}
