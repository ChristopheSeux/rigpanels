import bpy
import bpy.utils.previews
from os.path import join,basename,dirname,splitext
from os import listdir
from mathutils import Matrix


def get_armature():
    ob = bpy.context.object

    if not ob :
        return

    if ob.type == 'ARMATURE' :
        return ob
    else :
        armatures = [o for o in bpy.context.selected_objects if o.type == 'ARMATURE']

        if armatures :
            return armatures[0]
"""
def flatten_array(array,flat_list) :
    for a in array :
        if a["layers"] :
            flat_list.append(a["id"])
            flatten_array(a["layers"],flat_list)
        else :
            flat_list.append(a["id"])

    return flat_list
"""
"""
def flatten_layer_tree(tree,flat_list) :
    for l in tree :
        flat_list.append({'id' : l["id"],'parents':[],'children':[]})

        if l["layers"] :
            flat_list.append(l["id"])
            flatten_array(l["layers"],flat_list)
        else :
            flat_list.append({'id' : l["id"]})

    return flat_list
"""

def find_item(array,item_id) :
    item_list = list(filter(lambda x: x["id"]==item_id, array))

    if item_list:
        return {'array': array,'index' : array.index(item_list[0])}

    for a in array :
        if a['children'] :
            #parents.append(a["id"])

            item_found = find_item(a['children'],item_id)

            if item_found :
                return item_found


def get_children(array,children,recursive = False) :
    for a in array :
        children.append(a["id"])
        if recursive and a['children'] :
            item_found = get_children(a['children'],children)

    return children

'''
def get_children(array,) :
    item_info = find_item(array,item_id)
    return item_info['array'][item_info['id']]['children']
'''


"""
def get_children(array,children) :
    for a in array :
        children.append(a["id"])
        if a['children'] :
            item_found = get_children(a['children'],children)

    return children
    """


def remove_item(array,item_id) :
    item_info = find_item(array["children"],item_id)
    item_info["array"].pop(item_info["index"])

    return array


def move_item_in_group(array,src_id,dst_id) :
    src = find_item(array,src_id)
    dst = find_item(array,dst_id)

    src_item = src["array"][src["index"]].copy()

    dst["array"][dst["index"]]["children"].append(src_item)
    src["array"].pop(src["index"])



    return array

def move_item(array,id,step) :
    item_info = find_item(array["children"],id)
    item_array = item_info["array"]
    item_index = item_info["index"]

    item_array.insert(item_index+step, item_array.pop(item_index))


    return array


def title(name):
    title_name = name.replace('_',' ').lower()

    string = [i.upper() if i in ('ik','fk','ikfk','fkik') else i.title() for i in title_name.split(' ')]

    return ' '.join(string)


def find_mirror(name) :
    mirror = None
    prop= False

    if name :

        if name.startswith('[')and name.endswith(']'):
            prop = True
            name= name[:-2][2:]

        match={
        'R' : 'L',
        'r' : 'l',
        'L' : 'R',
        'l' : 'r',
        }

        separator=['.','_']

        if name.startswith(tuple(match.keys())):
            if name[1] in separator :
                mirror = match[name[0]]+name[1:]

        if name.endswith(tuple(match.keys())):
            if name[-2] in separator :
                mirror = name[:-1]+match[name[-1]]

        if mirror and prop == True:
            mirror='["%s"]'%mirror

        return mirror

    else :
        return None

def mirror_path(dp) :
    bone = split_path(dp)[0]
    prop = split_path(dp)[1]

    mirror_bone = find_mirror(bone)

    if not mirror_bone :
        mirror_bone = bone

    if prop :
        mirror_prop = find_mirror(prop)

        if not mirror_prop :
            mirror_prop = prop

        mirror_data_path = dp.replace('["%s"]'%prop,'["%s"]'%mirror_prop)

    mirror_data_path = dp.replace('["%s"]'%bone,'["%s"]'%mirror_bone)



    if dp!= mirror_data_path :
        return mirror_data_path

    else :
        return None

def split_path(path) :
    try :
        bone_name = path.split('["')[1].split('"]')[0]

    except :
        bone_name = None

    try :
        prop_name = path.split('["')[2].split('"]')[0]
    except :
        prop_name = None

    return bone_name,prop_name

def get_IK_bones(IK_last):
    ik_chain = IK_last.parent_recursive
    ik_len = 0

    #Get IK len :
    for c in IK_last.constraints :
        if c.type == 'IK':
            ik_len = c.chain_count -2
            break

    IK_root = ik_chain[ik_len]

    IK_mid= ik_chain[:ik_len]

    IK_mid.reverse()
    IK_mid.append(IK_last)

    return IK_root,IK_mid

def bone_list(chain):
    bones = []

    names = ['FK_root','FK_tip','IK_tip','IK_pole']

    for bone in chain.FK_mid :
        bones.append(bone.name)

    for name in names :
        bones.append(chain[name])
    return bones


def get_selected_bones() :
    selected_bones =[]
    if bpy.context.mode =='POSE':
        selected_bones = [b.bone for b in bpy.context.selected_pose_bones]
    elif bpy.context.mode =='EDIT_ARMATURE' :
        selected_bones = [b for b in bpy.context.selected_editable_bones]

    return selected_bones

def sort_layer(visible_layer):
    sorted_layer = []

    Column = {}
    for l in visible_layer :
        if not Column.get(l.column) :
            Column[l.column] = []

        Column[l.column].append(l)

    for col,layers in sorted(Column.items()) :
        sorted_layer.append(sorted(layers,key=lambda x : x.row))

    return sorted_layer

def update_col_index(collection) :
    for i,item in enumerate(collection) :
        item.col_index = i

def redraw_areas():
    for area in bpy.context.screen.areas :
        area.tag_redraw()

def get_icons():
    global custom_icons
    custom_icons = bpy.utils.previews.new()
    icons_dir = join(dirname(__file__),'icons')
    for icon in listdir(icons_dir) :
        custom_icons.load(splitext(icon)[0].upper(), join(icons_dir, icon), 'IMAGE',force_reload=True)

    custom_icons.update()
