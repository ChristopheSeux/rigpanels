import bpy
from .snapping_utils import *
from .utils import *


def unique_name(collection,name) :
    name_list = [l.name for l in collection]

    i = 1
    unique_name = name
    while unique_name in name_list :
        unique_name = "%s_%02d"%(name,i)
        i+=1

    #print(unique_name)
    return unique_name

def unique_id(collection):
    collection = [l["id"] for l in collection]

    i = 1
    while i in collection :
        i+=1

    return i

def unique_index(collection):
    collection = [l.index for l in collection]

    i = 0
    while i in collection :
        i+=1

    return i


def flatten_layer_tree(tree,parents,flat_dic,level = -1) :
    if flat_dic :
        index = max([v['index'] for k,v in flat_dic.items()])+1
    else :
        index = -1

    flat_dic[tree['id']] = {   "index" : index,
                                "level":level,
                                "parents" :parents.copy() ,
                                "children" : []}
    level+=1


    if tree["children"] :
        parents.append(tree["id"])

    for child in tree["children"] :

        flatten_layer_tree(child,parents,flat_dic,level)

        flat_dic[tree['id']]["children"].append(child['id'])
        for parent in flat_dic[child["id"]]["parents"] :
            if child['id'] not  in flat_dic[parent]["children"] :
                flat_dic[parent]["children"].append(child['id'])

    return flat_dic

def flatten_layer_tree(tree,parents,flat_dic,level = -1) :
    if flat_dic :
        index = max([v['index'] for k,v in flat_dic.items()])+1
    else :
        index = -1

    flat_dic[tree['id']] = {   "index" : index,
                                "level":level,
                                "parents" : parents.copy() ,
                                "children" : get_children(tree['children'],[],True),
                                'children_direct' : get_children(tree['children'],[],False)}
    level+=1

    for child in tree["children"] :

        flatten_layer_tree(child,parents+[tree["id"]],flat_dic,level)

        #flat_dic[tree['id']]["children"].append(child['id'])
        #for parent in flat_dic[child["id"]]["parents"] :
        #    if child['id'] not  in flat_dic[parent]["children"] :
        #        flat_dic[parent]["children"].append(child['id'])

    return flat_dic

"""
def layer_remove(layer) :
    ob = get_armature()
    BLayersSettings = ob.data.BLayers
    BLayers = BLayersSettings.layers

    layer_tree = eval(BLayersSettings["layer_tree"])
    item_info = find_item(layer_tree,layer['id'])
    layer_indices = {l["id"]:l for l in BLayers}

    for a in item_info["array"][item_info["index"]]["layers"] :
        layer = layer_indices[a["id"]]
        if a['layers'] :
            layer_remove(layer)

    id = layer["id"]
    print(layer,layer["id"])

    layer_index = list(BLayers).index(layer)
    BLayers.remove(layer_index)

    BLayersSettings["layer_tree"] = str(remove_item(layer_tree,id))
    return layer
"""

def layer_add(collection,type,index = None) :
    ob = get_armature()


    if not ob.data.BLayers.get("layer_tree") :
        ob.data.BLayers["layer_tree"] = "{'id':0,'children':[]}"


    id = unique_id(collection)
    layer = collection.add()


    layer_tree = eval(ob.data.BLayers["layer_tree"])

    layer["type"] = type
    layer["id"] = id

    if type == 'LAYER' :
        if not index :
            index = unique_index(collection)

        layer.index = index
        layer.name = unique_name(collection,type.title())

    elif type == 'GROUP':
        layer.name = unique_name(collection,type)

    layer_tree["children"].append({"id":id,"children":[]})

    ob.data.BLayers["layer_tree"] = str(layer_tree)

    return layer

def same_prop(collection,index,prop) :
    return [i for i,l in enumerate(collection) if getattr(l,prop) == getattr(collection[index],prop)]

def redraw_areas():
    for area in bpy.context.screen.areas :
        area.tag_redraw()

def create_layers():
    for scene in bpy.data.scenes :
        if not scene.BLayers.layers :
            src_layers = []
            for ob in scene.objects :
                for i,l in enumerate(ob.layers) :
                    if l and i not in src_layers :
                        src_layers.append(i)

            for l_index in sorted(src_layers) :
                l = scene.BLayers.layers.add()
                l.index = l_index
                l.name = 'Layer_%02d'%(l_index+1)

def move_layer_up(collection,index) :

    above_layer = reversed([i for i,l in enumerate(collection) if i< index])

    if collection[index].type == 'LAYER' and collection[index].id !=-1 :
        return index-1
    else :
        new_index =  0
        for i in above_layer :
            layer = collection[i]
            if layer.type == 'LAYER' and layer.id ==-1 or layer.type == 'GROUP' :
                new_index = i
                break

        return new_index

def move_layer_down(collection,index):
    #print('active_index',collection[i].id)
    if collection[index].type == 'LAYER':
        below_layer = [i for i,l in enumerate(collection) if i> index]
    elif collection[index].type == 'GROUP':
        below_layer = [i for i,l in enumerate(collection) if i> index if l.id !=collection[index].id]

    new_index =  len(collection)-1
    for i in below_layer :
        layer = collection[i]
        same_group= same_prop(collection,i,'id')

        if layer.type == 'LAYER' and layer.id ==-1  :
            new_index = i
            break

        elif layer.type == 'GROUP' and len(same_group)==1 :
            new_index = i
            break

        elif layer.type == 'LAYER' and layer.id !=-1 and i == max(same_group):
            new_index = i
            break

    return new_index

def insert_keyframe(bone,custom_prop=True):
    ob = bpy.context.object

    Transforms ={"location":("lock_location[0]","lock_location[1]","lock_location[2]"),
        "scale":("lock_scale[0]","lock_scale[1]","lock_scale[2]")}

    if bone.rotation_mode in 'QUATERNION':
        Transforms["rotation_quaternion"] = ("lock_rotation[0]","lock_rotation[1]","lock_rotation[2]","lock_rotation_w")

    elif bone.rotation_mode == 'AXIS_ANGLE':
        Transforms["rotation_axis_angle"] = ("lock_rotation[0]","lock_rotation[1]","lock_rotation[2]","lock_rotation_w")

    else :
        Transforms["rotation_euler"]=("lock_rotation[0]","lock_rotation[1]","lock_rotation[2]")

    if not ob.animation_data:
        ob.animation_data_create()

    for prop,lock in Transforms.items() :
        for index,channel in enumerate(lock) :
            #if the channel is not locked
            if not eval("bone."+channel) :
                ob.keyframe_insert(data_path = 'pose.bones["%s"].%s'%(bone.name,prop), index = index,group = bone.name)

        if custom_prop == True :
            for key,value in bone.items() :
                if key != '_RNA_UI' and key and type(value) in (int,float):
                    ob.keyframe_insert(data_path = 'pose.bones["%s"]["%s"]'%(bone.name,key) ,group = bone.name)



def snap_ik_fk(rig,way,switch_prop,
                    FK_root,FK_tip,
                    IK_last,IK_tip,IK_pole,
                    IK_stretch_last = None,
                    FK_mid=None,
                    full_snapping=True,
                    invert=False,
                    ik_fk_layer=None,
                    auto_switch=True):

    armature = rig.data
    poseBone = rig.pose.bones
    dataBone = rig.data.bones

    switch_bone,switch_prop_name = split_path(switch_prop)

    for c in IK_last.constraints :
        if c.type == 'IK':
            ik_len = c.chain_count
            break

    if not FK_mid :
        FK_mid = FK_tip.parent_recursive[:ik_len-1].reverse()

    IK_chain = ([IK_last]+IK_last.parent_recursive[:ik_len-1])[::-1]
    IK_root = IK_chain[0]
    FK_chain = ([FK_root]+FK_mid)[-ik_len:]

    if IK_stretch_last :
        IK_stretch_chain = ([IK_stretch_last]+IK_stretch_last.parent_recursive[:ik_len-1])[::-1]
        print('#### IK_stretch_chain',[b.name for b in IK_stretch_chain])
        print('')


    print('#### IK_chain',[b.name for b in IK_chain])
    #print(IK_stretch_chain)
    print('')
    print('#### FK_chain',[b.name for b in FK_chain])

    #######FK2IK
    if way == 'to_FK' :
        for i,FK_bone in enumerate(FK_chain) :
            if IK_stretch_last :
                match_bone = IK_stretch_chain[i]
            else :
                match_bone = IK_chain[i]

            match_matrix(FK_bone,match_bone)
            print('match',FK_bone,'to',match_bone)

        FK_tip.matrix = IK_tip.matrix
        FK_tip.location = (0,0,0)
        bpy.ops.pose.visual_transform_apply()
        #Rigify support
        if FK_root.get('stretch_length'):
            FK_root['stretch_length'] = IK_root.length/IK_root.bone.length

        invert_switch = invert*1.0

        if ik_fk_layer :
            layer_hide = ik_fk_layer[1]
            layer_show = ik_fk_layer[0]

        dataBone.active = FK_root.bone


    #######IK2FK
    elif way == 'to_IK' :
        #mute IK constraint
        for c in IK_last.constraints :
            if c.type == 'IK' :
                c.mute = True

        IK_tip.matrix = FK_tip.matrix
        bpy.ops.pose.visual_transform_apply()

        if full_snapping :
            for i,IK_bone in enumerate(IK_chain) :
                match_matrix(IK_bone,FK_chain[i])

        for c in IK_last.constraints :
            if c.type == 'IK' :
                c.mute = False
        bpy.ops.pose.visual_transform_apply()

        #else :
        match_pole_target(IK_chain[0],IK_last,IK_pole,FK_chain[0],(IK_root.length+IK_last.length))
        bpy.ops.pose.visual_transform_apply()


        invert_switch = (not invert)*1.0
        #setattr(IKFK_chain,'layer_switch',0)

        dataBone.active = IK_tip.bone

        if ik_fk_layer :
            layer_hide = ik_fk_layer[0]
            layer_show = ik_fk_layer[1]

    if ik_fk_layer and auto_switch:
        setattr(poseBone.get(switch_bone),'["%s"]'%switch_prop_name,invert_switch)

        rig.data.layers[layer_hide] = False
        rig.data.layers[layer_show] = True

    ###settings keyframe_points
    keyBone = (FK_root,FK_tip,IK_tip,IK_pole)

    if bpy.context.scene.tool_settings.use_keyframe_insert_auto:
        if not rig.animation_data:
            rig.animation_data_create()

        rig.keyframe_insert(data_path=switch_prop,group=switch_bone)

        for b in keyBone :
            insert_keyframe(b)
        for b in FK_mid :
            insert_keyframe(b)
