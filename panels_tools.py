import bpy
from . import utils
from .utils import get_armature,find_item
from .functions import flatten_layer_tree

##Menu
class BLayerTypeMenu(bpy.types.Menu):
    """Add Group or Layer"""
    bl_label = "Choose Layer type"

    def draw(self, context):
        layout = self.layout
        scene = bpy.context.scene

        layout.operator("blayers.add_layer", icon='NEW',text = 'Add layer').type = 'LAYER'
        icon = scene.BLayers.icons["NEW_LAYER_OBJECT"].icon_id
        layout.operator("blayers.add_layer", icon_value=icon,text = 'Add layer from object').type = 'LAYER_FROM_SELECTED'
        layout.operator("blayers.add_layer", icon='NEWFOLDER',text = 'Add Group').type = 'GROUP'


class BLayerSpecialMenu(bpy.types.Menu):
    """Special Menu"""
    bl_label = "Special Layer menu"

    def draw(self, context):
        layout = self.layout

        #layout.operator("blayers.select_objects", icon='RESTRICT_SELECT_OFF', text="Select Objects")
        layout.operator("blayers.synchronise_layers", icon='FILE_REFRESH', text="Synchronise Layers")
        layout.prop(context.scene.BLayers,'show_index')

        layout.operator("blayers.copy_layers", icon='COPYDOWN', text="Copy Layers")
        layout.operator("blayers.paste_layers", icon='PASTEDOWN', text="Paste Layers")

        layout.separator()
        layout.operator("blayers.objects_to_layer", icon='HAND', text="Move to Layers")

##Layer List
class BLayersList(bpy.types.UIList):
    use_filter_show = True
    def draw_filter(self, context, layout):
        ob = get_armature()
        row = layout.row(align = True)
        row.prop(self,'filter_name',icon = 'VIEWZOOM',text="")

        row.separator()
        row.prop(ob,"show_x_ray",icon = 'OUTLINER_DATA_ARMATURE',text='')
        row.prop(ob.data,"show_names",icon = 'SORTALPHA',text='')

    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        ob = get_armature()
        scene = context.scene

        #self.use_filter_show = True

        BLayers = ob.data.BLayers.layers
        layers = ob.data.layers

        active_bone = context.active_bone

        view_3d = context.area.spaces.active  # Ensured it is a 'VIEW_3D' in panel's poll(), weak... :/
        use_spacecheck = view_3d.lock_camera_and_layers

        row = layout.row(align=True)


        layer_tree = eval(ob.data.BLayers["layer_tree"])
        flat_layer_tree =  flatten_layer_tree(layer_tree,[],{})


        #print("flat_layer_tree",flat_layer_tree)
        if not flat_layer_tree.get(item['id']) :
            return
        item_info = flat_layer_tree[item['id']]


        if item["type"] == 'GROUP' :
            visibility_icon = 'VISIBLE_IPO_ON' if item.visibility else 'VISIBLE_IPO_OFF'
            expand_icon = utils.custom_icons["GROUP_OPEN"].icon_id if item.expand else utils.custom_icons["GROUP_CLOSED"].icon_id
            row.prop(item,'visibility',icon =visibility_icon ,text='', emboss=False)


            for i in range(item_info["level"]) :
                row.label(icon="BLANK1")

            row.prop(item,'expand',icon_value =expand_icon ,text='', emboss=False)
            row.prop(item, "name", text="", emboss=False )
            row.separator()
            row.operator("blayers.move_in_group",icon_value =utils.custom_icons["IN_GROUP"].icon_id ,text='',emboss= False).index = index

            icon = "LOCKED" if item.lock_group else "UNLOCKED"
            op = row.prop(item,"lock_group", text="", emboss=False, icon=icon)

            if icon == 'LOCKED' :
                row.active = False

        elif item["type"]== "LAYER" : #item type = LAYER
            layer_used_icon = 'BLANK1'
            layer_on = layers[item.index]
            active_ob_on_layer = active_bone and active_bone.layers[item.index]
            ob_on_layer = [o for o in ob.data.bones if o.layers[item.index]]

            if use_spacecheck :
                icon = 'RESTRICT_VIEW_OFF' if layer_on else 'RESTRICT_VIEW_ON'
                row.prop(ob.data,"layers",index = item.index, text="", emboss=False, icon=icon)

            else :
                icon = 'RESTRICT_VIEW_OFF' if view_3d.layers[item.index] else 'RESTRICT_VIEW_ON'
                row.prop(view_3d,"layers",index = item.index, text="", emboss=False, icon=icon)


            for i in range(item_info["level"]-1) :
                row.label(icon="BLANK1")

            if item_info["level"] :
                row.label(icon_value=utils.custom_icons["GROUP_TREE"].icon_id)
            #if item.id in [l.id for l in BLayers if l.type == 'GROUP'] :
            #    row.label(icon_value=utils.custom_icons["GROUP_TREE"].icon_id)

            row.prop(item, "name", text="", emboss=False)

            if active_ob_on_layer :
                row.label(icon='LAYER_ACTIVE')

            elif ob_on_layer:
                row.label(icon='LAYER_USED')

            icon = "LOCKED" if item.lock else "UNLOCKED"
            op = row.prop(item,"lock", text="", emboss=False, icon=icon)

            if icon == 'LOCKED' :
                row.active = False

        if scene.BLayers.show_index :
            row.prop(item,"index",text = '')

        #row.label(str(item['id']))

    def filter_items(self, context, data, propname):
        scene = context.scene
        ob = get_armature()

        BLayers = ob.data.BLayers.layers
        layers = ob.data.layers


        layers_indices = [l["id"] for l in BLayers]

        layer_closed = [l['id'] for l in BLayers if not l.expand]

        layer_tree =  eval(ob.data.BLayers["layer_tree"])

        flat_layer_tree = flatten_layer_tree(layer_tree,[],{})

        #print('# flat_layer_tree')
        #print(flat_layer_tree)


        flt_flags = [self.bitflag_filter_item] * len(BLayers)

        for i,layer in enumerate(BLayers):
            parents = flat_layer_tree[layer["id"]]["parents"]

            if list(set(parents) & set(layer_closed)) :
                flt_flags[i] = 0

        #print('###')

        #print([flat_layer_tree[id]['index'] for id in layers_indices])

        return flt_flags,[flat_layer_tree[id]['index'] for id in layers_indices]


'''
    def filter_items(self, context, data, propname):
        scene = context.scene
        ob = get_armature()

        BLayers = ob.data.BLayers.layers
        layers = ob.data.layers

        layers = getattr(data, propname)
        helper_funcs = bpy.types.UI_UL_list

        # Default return values.
        flt_flags = []
        flt_neworder = []

        # Filtering by name
        if self.filter_name:
            flt_flags = helper_funcs.filter_items_by_name(self.filter_name, self.bitflag_filter_item, layers, "name",
                                                          reverse=self.use_filter_name_reverse)
        if not flt_flags:
            flt_flags = [self.bitflag_filter_item] * len(layers)

        for i,layer in enumerate(layers):
            groups = [l for l in BLayers if l.type=='GROUP' and l.id == layer.id]
            if layer.type == 'LAYER' and groups and not groups[0].expand:
                flt_flags[i] = 0

        if self.use_filter_sort_alpha:
            flt_neworder = helper_funcs.sort_items_by_name(layers, "name")

        return flt_flags,[]
'''
#Panels
class LayerPanel(bpy.types.Panel) :
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_label = "Rig Layers"
    bl_category = "Rig Layers"

    @classmethod
    def poll(self,context) :
        return get_armature()

    @staticmethod
    def draw(self, context):
        layout = self.layout

        ob = get_armature()
        scene = context.scene

        BLayersSettings = ob.data.BLayers
        BLayers = ob.data.BLayers.layers

        main_row = layout.row()
        #box_row = box.row(align = True)
        left_col = main_row.column(align = True)
        right_col = main_row.column(align= True)


        if BLayers  :
            left_col.template_list("BLayersList", "", BLayersSettings, "layers", BLayersSettings, "active_index", rows=9)
        else :
            left_col.operator("blayers.synchronise_layers", icon='FILE_REFRESH', text="Synchronise Layers")

        right_col.menu("BLayerTypeMenu", icon="ZOOMIN", text="")
        right_col.operator("blayers.remove_layer", icon ='ZOOMOUT', text="")
        right_col.menu("BLayerSpecialMenu", icon="DOWNARROW_HLT", text="")
        right_col.separator()
        right_col.operator("blayers.layer_move", icon='TRIA_UP', text="").step = -1
        right_col.operator("blayers.layer_move", icon='TRIA_DOWN', text="").step = 1

        right_col.separator()
        right_col.operator("blayers.toogle_layer_lock", icon='LOCKED', text="")
        right_col.operator("blayers.toogle_layer_hide", icon='RESTRICT_VIEW_OFF', text="")
        right_col.separator()
        right_col.operator("blayers.select_objects", icon='RESTRICT_SELECT_OFF', text="")

        if BLayers and BLayersSettings.active_index !=-1:
            right_col.operator("blayers.objects_to_layer", icon='HAND', text="").layer_index = BLayers[BLayersSettings.active_index].index
