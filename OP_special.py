import bpy

from .functions import *


class CreateBoneGroup(bpy.types.Operator) :
    """Layer Preset Management"""
    bl_idname = "blayers.load_bone_group"
    bl_label = "Add or Remove Layers Preset"

    def set_attributes(self,ob,info) :
        for attr,value in info.items() :
            if isinstance(value,dict) :
                self.set_attributes(getattr(ob,attr),value)
            else :
                setattr(ob, attr,value)


    def execute(self, context):
        bone_groups = [
        {"name":'Root','color_set' : 'CUSTOM',"colors":{'normal':(0,0,0),'select': (0,1,1),'active': (1,1,1)}},
        {"name":'Spine','color_set' : 'CUSTOM',"colors":{'normal':(1,1,0),'select': (0,1,1),'active': (1,1,1)}},
        {"name":'Left','color_set' : 'CUSTOM',"colors":{'normal':(0,0.082,0.914),'select': (0,1,1),'active': (1,1,1)}},
        {"name":'Right','color_set' : 'CUSTOM',"colors":{'normal':(0.969,0.039,0.039),'select': (0,1,1),'active': (1,1,1)}},
        {"name":'IK.L','color_set' : 'CUSTOM',"colors":{'normal':(0.671,0,0.867),'select': (0,1,1),'active': (1,1,1)}},
        {"name":'IK.R','color_set' : 'CUSTOM',"colors":{'normal':(1,0,0.553),'select': (0,1,1),'active': (1,1,1)}},
        {"name":'Tweak.L','color_set' : 'CUSTOM',"colors":{'normal':(0,0.659,0.753),'select': (0,1,1),'active': (1,1,1)}},
        {"name":'Tweak.R','color_set' : 'CUSTOM',"colors":{'normal':(1,0.608,0),'select': (0,1,1),'active': (1,1,1)}},
        ]

        ob = context.object
        for bone_group_info in bone_groups :
            g = ob.pose.bone_groups.get(bone_group_info["name"])
            if not g :
                g = ob.pose.bone_groups.new()

            self.set_attributes(g, bone_group_info)


        return {'FINISHED'}
